# zsh-config

- For Arch Linux

## Requirements
- [fzf](https://github.com/junegunn/fzf)
- [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)
## Optional
- [zsh-autosuggestion](https://github.com/zsh-users/zsh-autosuggestions/tree/master)
- [Starship](https://github.com/starship/starship)
- [pywal](https://github.com/dylanaraps/pywal)

## Instructions
Set $ZDOTDIR
```
echo 'export ZDOTDIR="$HOME/.config/zsh"' >> $HOME/.zprofile
```
Clone config
```
git clone --depth 1 https://codeberg.org/Tromzz/zsh-config.git ~/.config/zsh
```
- Run 'zsh' command and test basic commands to make sure zsh is working.

## Changing default shell to zsh
Locate zsh file path and append to /etc/shells
```
command -v zsh | sudo tee -a /etc/shells
```
Set ZSH as default
```
sudo chsh -s $(which zsh) $USER
```